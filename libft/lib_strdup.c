/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_strdup.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbijon <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/22 17:00:01 by mbijon            #+#    #+#             */
/*   Updated: 2018/09/23 17:27:36 by mbijon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

char		*lib_strdup(const char *str)
{
	int		i;
	char	*dst;

	i = -1;
	if ((dst = (char *)malloc(sizeof(char) * (lib_strlen(str) + 1))) == NULL)
		return (NULL);
	if (str != NULL)
		while (str[++i])
			dst[i] = str[i];
	dst[i] = '\0';
	return (dst);
}
