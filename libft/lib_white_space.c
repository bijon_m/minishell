/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_white_space.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbijon <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/26 19:48:46 by mbijon            #+#    #+#             */
/*   Updated: 2018/06/29 18:34:15 by mbijon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

static char	*lib_strdup_space(const char *src, int pos, const int size)
{
	char	*dst;
	int		i;

	i = 0;
	if ((dst = (char *)malloc(sizeof(char) * (size + 1))) == NULL)
		return (NULL);
	while (i < size)
		dst[i++] = src[pos++];
	dst[i] = '\0';
	return (dst);
}

char		**lib_white_space(const char *line, const char sep)
{
	char	**tab;
	int		i;
	int		pos;
	int		j;

	i = 0;
	j = 0;
	if ((tab = (char **)malloc(sizeof(char *) * (lib_count_char_str(line,
						sep) + 2))) == NULL)
		return (NULL);
	while (line[i])
	{
		pos = i;
		while (line[i] && line[i] != sep)
			i++;
		if ((tab[j++] = lib_strdup_space(line, pos, i - pos)) == NULL)
			return (NULL);
		if (line[i])
			i++;
	}
	tab[j] = NULL;
	return (tab);
}
