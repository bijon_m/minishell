/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbijon <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/20 02:21:57 by mbijon            #+#    #+#             */
/*   Updated: 2018/10/08 15:19:57 by mbijon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H

int		lib_count_line_tab(const char **tab);

char	**lib_copy_double_tab(const char **old_tab, const int size);

char	*lib_strdup(const char *str);

char	*lib_strndup(char *str);

int		lib_strlen(const char *str);

void	lib_putstr(const int fd, const char *str);

int		lib_count_alpha(const char *src);

char	*lib_epur_str(const char *src);

int		lib_count_epur(const char c);

char	**lib_white_space(const char *line, const char sep);

char	*lib_strnchr(char *src, const int size, const char sep);

int		lib_count_char_str(const char *str, const char sep);

int		lib_strcmp(const char *s1, const char *s2);

int		lib_get_nbr(const char *str);

void	lib_put_tab(const char **tab);

void	lib_free_tab(char **tab);

char	**lib_copy_tab_line(char **old_tab, const char *line);

char	**lib_copy_without_line(char **tab, int ret);

int		lib_line_env(const char *env, const char *line);

char	*lib_strncat(const char *src, const char c, const char *ptr,
					const int size);

char	*lib_strccat(const char *src, const char c, const char *ptr,
					const int size);

int		lib_count_str_str(const char *src, const char *patern);

char	*lib_strrchr(const char *src, const char sep);

char	*lib_strcat(const char *dst, const char *src);

int		lib_pos_tab(const char *src, const char **env);

char	*lib_path(const char **env);

void	lib_putnbr(int nb);

#endif
