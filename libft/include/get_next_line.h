/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbijon <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/22 11:09:31 by mbijon            #+#    #+#             */
/*   Updated: 2018/06/23 02:11:27 by mbijon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H

# ifndef BUFF_SIZE
#  define BUFF_SIZE		2
# endif

int						lib_get_next_line(const int fd, char **line);

typedef struct			s_gnl
{
	int					len_total;
	int					len_read;
	int					len_line;
	char				*save;
}						t_gnl;

#endif
