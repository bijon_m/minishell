/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_copy_tab_line.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbijon <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/30 01:08:26 by mbijon            #+#    #+#             */
/*   Updated: 2018/10/13 15:15:02 by mbijon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

char		**lib_copy_tab_line(char **old_tab, const char *line)
{
	int		i;
	char	**tab;

	if ((tab = (char **)malloc(sizeof(char *) * (lib_count_line_tab((const
		char **)old_tab) + 2))) == NULL)
		return (NULL);
	i = 0;
	if (old_tab != NULL)
	{
		while (old_tab[i])
		{
			tab[i] = lib_strdup(old_tab[i]);
			i++;
		}
		lib_free_tab(old_tab);
	}
	tab[i++] = lib_strdup(line);
	tab[i] = NULL;
	return (tab);
}
