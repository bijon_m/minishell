/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcat.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbijon <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/10 11:53:27 by mbijon            #+#    #+#             */
/*   Updated: 2018/09/25 13:35:47 by mbijon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

char		*lib_strcat(const char *dest, const char *src)
{
	int		i;
	int		j;
	char	*tmp;

	i = 0;
	j = 0;
	if ((tmp = (char *)malloc(sizeof(char) * (lib_strlen(dest) +
		lib_strlen(src) + 1))) == NULL)
		return (NULL);
	while (dest[i])
	{
		tmp[i] = dest[i];
		i++;
	}
	if (src != NULL)
	{
		while (src[j])
			tmp[i++] = src[j++];
	}
	tmp[i] = '\0';
	return (tmp);
}
