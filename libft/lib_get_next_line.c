/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbijon <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/02 12:08:23 by mbijon            #+#    #+#             */
/*   Updated: 2018/06/29 20:18:33 by mbijon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <unistd.h>
#include "get_next_line.h"

static int			ft_count(const char *str, int n)
{
	int				i;

	i = 0;
	if (str != NULL)
		while (str[i] != '\n' && i < n)
			i++;
	return (i);
}

static char			*ft_strndup(char *src, int size)
{
	int				i;
	char			*tmp;

	i = 0;
	if (src == NULL)
		return (NULL);
	if ((tmp = (char *)malloc(sizeof(char) * (size + 1))) == NULL)
		return (NULL);
	while (i < size)
	{
		tmp[i] = src[i];
		i++;
	}
	tmp[i] = '\0';
	return (tmp);
}

static char			*ft_realloc(t_gnl *elem, char *buf, int pos)
{
	char			*tmp;
	int				i;
	int				j;

	i = 0;
	j = 0;
	if ((tmp = (char *)malloc(sizeof(char) * (elem->len_read + elem->len_total
		+ 2))) == NULL)
		return (NULL);
	if (elem->save != NULL)
	{
		while (i < elem->len_total)
			tmp[i++] = elem->save[pos++];
		free(elem->save);
	}
	if (buf != NULL)
		while (j <= elem->len_read)
			tmp[i++] = buf[j++];
	tmp[i] = '\0';
	return (tmp);
}

static int			ret_get(t_gnl *elem, char **line, int ret_new_line)
{
	if (elem->len_read < 0)
		return (-1);
	*line = ft_strndup(elem->save, ret_new_line);
	if ((elem->len_total - ret_new_line) > 0)
	{
		elem->len_total -= ret_new_line + 1;
		elem->len_read = 0;
		if ((elem->save = ft_realloc(elem, NULL, ret_new_line + 1)) == NULL)
			return (-1);
	}
	else
	{
		if (elem->save != NULL)
		{
			free(elem->save);
			elem->save = NULL;
		}
	}
	return (1);
}

int					lib_get_next_line(const int fd, char **line)
{
	static t_gnl	elem = (t_gnl) {0, 0, 0, NULL};
	char			buf[BUFF_SIZE];

	if (elem.save == NULL)
		elem.len_total = 0;
	if (fd == -1 || (read(fd, "", 0) < 0) || BUFF_SIZE < 0 || line == NULL)
		return (-1);
	while ((elem.len_read = read(fd, buf, BUFF_SIZE)) > 0)
	{
		if ((elem.save = ft_realloc(&elem, buf, 0)) == NULL)
			return (-1);
		elem.len_total += elem.len_read;
		if ((elem.len_line = ft_count(elem.save, elem.len_total))
				< elem.len_total)
			return (ret_get(&elem, line, elem.len_line));
	}
	if (elem.len_total > 0)
		return (ret_get(&elem, line, ft_count(elem.save, elem.len_total)));
	*line = NULL;
	if (elem.save != NULL)
	{
		free(elem.save);
		elem.save = NULL;
	}
	return (0);
}
