/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_strccat.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbijon <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/14 11:20:27 by mbijon            #+#    #+#             */
/*   Updated: 2018/09/25 14:51:16 by mbijon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>
#include "libft.h"

char		*lib_strccat(const char *src, const char c, const char *ptr,
						const int size)
{
	char	*tmp;
	int		i;
	int		j;

	i = 0;
	j = 0;
	if ((tmp = (char *)malloc(sizeof(char) * (lib_strlen(src) +
						lib_strlen(ptr) + size))) == NULL)
		return (NULL);
	if (src != NULL)
		while (src[i])
			tmp[j++] = src[i++];
	tmp[j++] = c;
	i = 0;
	if (ptr != NULL)
	{
		while (ptr[i])
			tmp[j++] = ptr[i++];
	}
	tmp[j] = '\0';
	return (tmp);
}
