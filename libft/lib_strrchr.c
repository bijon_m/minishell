/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbijon <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/10 11:51:04 by mbijon            #+#    #+#             */
/*   Updated: 2017/11/10 11:51:06 by mbijon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

static int	my_strlen(const char *str)
{
	int		i;

	i = 0;
	while (str[i])
		i++;
	return (i);
}

char		*lib_strrchr(const char *s, int c)
{
	int		i;

	i = my_strlen(s);
	while (i >= 0)
	{
		if (s[i] == c)
			return ((char *)&s[i + 1]);
		i--;
	}
	return (NULL);
}
