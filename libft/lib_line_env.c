/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_line_env.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbijon <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/14 11:18:03 by mbijon            #+#    #+#             */
/*   Updated: 2018/09/14 11:18:06 by mbijon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int		lib_line_env(const char *env, const char *line)
{
	int	i;

	i = 0;
	if (env != NULL && line != NULL)
		while (env[i] && line[i] && line[i] == env[i])
		{
			if (line[i + 1] == '\0')
				return (0);
			i++;
		}
	return (-1);
}
