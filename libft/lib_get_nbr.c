/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_get_nbr.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbijon <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/29 18:21:56 by mbijon            #+#    #+#             */
/*   Updated: 2018/09/14 11:17:45 by mbijon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int			lib_get_nbr(const char *str)
{
	int		res;
	int		sign;
	int		i;

	res = 0;
	sign = 1;
	i = 0;
	if (str == NULL)
		return (res);
	if (str[i] == '-')
	{
		sign = -1;
		i += 1;
	}
	while (str[i])
		res = (res * 10) + str[i++] - 48;
	return (res * sign);
}
