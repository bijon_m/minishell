/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_count_str_str.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbijon <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/14 11:07:23 by mbijon            #+#    #+#             */
/*   Updated: 2018/09/14 11:11:54 by mbijon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

int			lib_count_str_str(const char *src, const char *patern)
{
	int		count;
	int		i;
	int		size;

	count = 0;
	i = 0;
	size = lib_strlen(patern);
	if (src != NULL && patern != NULL)
		while (src[i] && src[i + 1])
		{
			if (lib_strcmp(&src[i], patern) == 0)
			{
				i += size;
				count++;
			}
			i++;
		}
	return (count);
}
