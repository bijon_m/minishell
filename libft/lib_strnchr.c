/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_strnchr.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbijon <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/14 11:21:13 by mbijon            #+#    #+#             */
/*   Updated: 2018/09/23 15:31:32 by mbijon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

static char		*lib_strndup(char *src, const int i)
{
	char		*tmp;
	int			j;

	j = 0;
	if ((tmp = (char *)malloc(sizeof(char) * (i + 1))) == NULL)
		return (NULL);
	while (src[i] && j < i)
	{
		tmp[j] = src[j];
		j++;
	}
	free(src);
	return (tmp);
}

char			*lib_strnchr(char *src, const int size,
							const char sep)
{
	int			i;
	int			count;

	i = 0;
	count = 0;
	if (src == NULL)
		return (NULL);
	while (src[i] && count <= size)
	{
		if (src[i] == sep)
			count++;
		i++;
	}
	return (lib_strndup(src, i));
}
