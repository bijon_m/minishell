/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_copy_without_line.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbijon <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/12 02:42:43 by mbijon            #+#    #+#             */
/*   Updated: 2018/09/14 11:10:55 by mbijon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

char		**lib_copy_without_line(char **tab, int ret)
{
	int		i;
	int		j;
	char	**tmp;

	i = 0;
	j = 0;
	if ((tmp = (char **)malloc(sizeof(char *) *
			lib_count_line_tab((const char **)tab))) == NULL)
		return (NULL);
	if (tab != NULL)
	{
		while (tab[i])
		{
			if (i != ret)
				tmp[j++] = lib_strdup(tab[i]);
			i++;
		}
		lib_free_tab(tab);
	}
	tmp[j] = NULL;
	return (tmp);
}
