/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_whites_space.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbijon <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/19 04:48:26 by mbijon            #+#    #+#             */
/*   Updated: 2018/07/12 01:06:23 by mbijon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

char		**lib_copy_double_tab(const char **old_tab, const int size)
{
	char	**tab;
	int		i;

	i = 0;
	if ((tab = (char **)malloc(sizeof(char *) * (size + 1))) == NULL)
		return (NULL);
	if (old_tab != NULL)
		while (old_tab[i])
		{
			tab[i] = lib_strdup(old_tab[i]);
			i++;
		}
	tab[i] = NULL;
	return (tab);
}
