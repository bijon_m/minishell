/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_pos_tab.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbijon <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/15 13:15:58 by mbijon            #+#    #+#             */
/*   Updated: 2018/09/22 13:08:07 by mbijon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int			lib_pos_tab(const char *src, const char **env)
{
	int		i;

	i = 0;
	while (env[i])
	{
		if (lib_strcmp(src, env[i]) == 0)
			return (i);
		i++;
	}
	return (0);
}
