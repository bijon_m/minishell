/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_epur_str.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbijon <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/23 03:47:31 by mbijon            #+#    #+#             */
/*   Updated: 2018/09/14 11:16:41 by mbijon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

static int			lib_count_char_epur(const char *str)
{
	int		i;
	int		count;

	i = -1;
	count = 0;
	while (str[++i])
		if (lib_count_epur(str[i]) == 1)
			count++;
	return (count);
}

int					lib_count_epur(const char c)
{
	if (c != ' ' && c != '\v' && c != '\n' && c != '\t')
		return (1);
	return (0);
}

static int			ft_count_word(const char *src)
{
	int		i;
	int		count;

	i = -1;
	count = 0;
	while (src[++i])
		if (i > 0 && lib_count_epur(src[i - 1]) == 1 && lib_count_epur(src[i])
				== 0)
			count++;
	if (lib_count_epur(src[i - 1]) == 0)
		--count;
	return (count + 1);
}

char				*lib_epur_str(const char *src)
{
	char	*epur;
	int		i;
	int		j;
	int		ret;

	i = -1;
	j = 0;
	if ((ret = lib_count_char_epur(src)) == 0)
		return (NULL);
	if ((epur = (char *)malloc(sizeof(char) * (ft_count_word(src) +
		lib_count_char_epur(src)) + 2)) == NULL)
		return (NULL);
	while (src[++i])
	{
		while (src[i + 1] && src[i] && lib_count_epur(src[i]) == 0)
			i++;
		if (j > 0 && lib_count_epur(src[i - 1]) == 0)
			epur[j++] = ' ';
		epur[j++] = src[i];
	}
	epur[j] = '\0';
	return (epur);
}
