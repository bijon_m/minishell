/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_strncat.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbijon <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/08 15:19:25 by mbijon            #+#    #+#             */
/*   Updated: 2018/10/08 15:19:28 by mbijon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

char		*lib_strncat(const char *src, const char c, const char *ptr,
						const int size)
{
	char	*tmp;
	int		i;
	int		j;

	i = 0;
	j = 0;
	if ((tmp = (char *)malloc(sizeof(char) * (lib_strlen(src) +
						lib_strlen(ptr) + size))) == NULL)
		return (NULL);
	if (src != NULL)
	{
		while (src[i])
			tmp[j++] = src[i++];
		free((void *)src);
	}
	tmp[j++] = c;
	i = 0;
	if (ptr != NULL)
	{
		while (ptr[i])
			tmp[j++] = ptr[i++];
		free((void *)ptr);
	}
	tmp[j] = '\0';
	return (tmp);
}
