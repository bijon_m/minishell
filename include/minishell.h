/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbijon <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/19 03:59:19 by mbijon            #+#    #+#             */
/*   Updated: 2018/10/13 15:06:29 by mbijon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINISHELL_H
# define MINISHELL_H

# ifndef PATH_MAX
#  define PATH_MAX	5000000
# endif

typedef struct	s_built
{
	int			ret_exit;
}				t_built;

typedef struct	s_mini
{
	char		**env;
	char		**av;
	int			ac;
	int			len_ret;
	t_built		built;
}				t_mini;

typedef struct	s_echo
{
	int			e;
	int			n;
	int			e_maj;
}				t_echo;

typedef struct	s_tab
{
	char		flag;
	void		(*f)(void);
}				t_tab;

char			*ft_manage_line(const char *buf, t_mini *elem);

int				ft_manage_exit(t_mini *elem);

int				ft_built_env(t_mini *elem);

int				ft_built_unsetenv(t_mini *elem);

int				ft_built_setenv(t_mini *elem);

int				ft_built_cd(t_mini *elem);

int				ft_built_echo(t_mini *elem);

int				ft_check_tab_line(const char **src, const char *str);

int				ft_pos_path(const char **tab);

int				ft_option_echo(t_mini *elem, t_echo *nv);

void			ft_msg_error_executable(t_mini *elem, char **tab);

void			ft_free_struct(t_mini *elem);

int				ft_manage_executable(t_mini *elem);

char			*ft_less(void);

int				ft_msg_error_cd(char *nv_path, t_mini *elem);

void			is_bell(void);

void			is_slash(void);

void			is_back(void);

void			is_escape(void);

void			is_formfeed(void);

void			is_newline(void);

void			is_return(void);

void			is_horizontal(void);

void			is_vertical(void);

#endif
