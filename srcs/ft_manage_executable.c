/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_manage_executable.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbijon <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/08 17:00:24 by mbijon            #+#    #+#             */
/*   Updated: 2018/10/13 15:10:00 by mbijon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include "get_next_line.h"
#include "libft.h"
#include "minishell.h"

int			ft_executable(const char *path, t_mini *elem, int *ret)
{
	pid_t	pid;

	if ((pid = fork()) == -1)
		return (-2);
	if (pid > 0)
	{
		waitpid(pid, ret, WUNTRACED);
		kill(pid, SIGCONT);
	}
	else
		exit(execve(path, elem->av, elem->env));
	return (*ret);
}

int			ft_condition_executable(t_mini *elem, int ret, const char *path)
{
	int		i;
	char	**tab;
	char	*tmp;

	i = 0;
	if ((tab = lib_white_space(path, ':')) == NULL)
		return (2);
	while (tab[i])
	{
		tmp = lib_strccat(tab[i], '/', elem->av[0], 2);
		if (ft_executable(tmp, elem, &ret) == 0)
		{
			free(tmp);
			lib_free_tab(tab);
			return (0);
		}
		free(tmp);
		i++;
	}
	ft_msg_error_executable(elem, tab);
	return (2);
}

int			ft_manage_executable(t_mini *elem)
{
	char	*path;
	int		ret;

	ret = 0;
	if ((path = lib_path((const char **)elem->env)) != NULL &&
			lib_count_char_str(elem->av[0], '/') == 0)
		return (ft_condition_executable(elem, ret, path));
	else
	{
		if (ft_executable(elem->av[0], elem, &ret) == 0)
			return (0);
		else
			ft_msg_error_executable(elem, NULL);
	}
	return (1);
}
