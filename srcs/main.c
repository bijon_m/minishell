/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbijon <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/08 16:57:21 by mbijon            #+#    #+#             */
/*   Updated: 2018/10/13 14:57:08 by mbijon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>
#include "get_next_line.h"
#include "libft.h"
#include "minishell.h"

static void		ft_init_struct(t_mini *elem, const char *env[])
{
	elem->av = NULL;
	elem->env = NULL;
	elem->env = lib_copy_double_tab(env, lib_count_line_tab(env));
	elem->built.ret_exit = 0;
}

int				ft_ret_exit(char *buf, char *epur, t_mini *elem)
{
	if (elem->env == NULL)
		return (1);
	free(buf);
	buf = NULL;
	free(epur);
	epur = NULL;
	ft_free_struct(elem);
	return (elem->built.ret_exit);
}

void			ft_condition(t_mini *elem)
{
	if (ft_built_env(elem) == 2 && ft_built_unsetenv(elem) == 2
		&& ft_built_setenv(elem) == 2 && ft_built_cd(elem) == 2
		&& ft_built_echo(elem) == 2)
		ft_manage_executable(elem);
}

int				main(int ac, const char *av[], const char *env[])
{
	t_mini	elem;
	char	*buf;
	char	*epur;

	(void)ac;
	(void)av;
	buf = NULL;
	ft_init_struct(&elem, env);
	lib_putstr(1, "$> ");
	while (lib_get_next_line(0, &buf) != 0)
	{
		if ((epur = ft_manage_line(buf, &elem)) != NULL)
		{
			if (ft_manage_exit(&elem) == 2)
				return (ft_ret_exit(buf, epur, &elem));
			ft_condition(&elem);
			free(epur);
			epur = NULL;
			lib_free_tab(elem.av);
		}
		free(buf);
		buf = NULL;
		lib_putstr(1, "$> ");
	}
	return (0);
}
