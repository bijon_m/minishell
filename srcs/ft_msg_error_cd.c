/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_msg_error_cd.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbijon <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/13 15:00:56 by mbijon            #+#    #+#             */
/*   Updated: 2018/10/13 15:08:54 by mbijon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "minishell.h"
#include "libft.h"

int		ft_msg_error_cd(char *nv_path, t_mini *elem)
{
	char	*tmp;

	free(nv_path);
	lib_putstr(2, "cd: no such file or directory: ");
	tmp = lib_strcat(elem->av[1], "\n");
	lib_putstr(2, tmp);
	free(tmp);
	return (-1);
}
