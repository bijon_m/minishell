/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_built_unsetenv.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbijon <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/15 12:51:13 by mbijon            #+#    #+#             */
/*   Updated: 2018/10/08 15:32:43 by mbijon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "minishell.h"
#include "libft.h"

static int	ft_msg_error(t_mini *elem)
{
	lib_putstr(2, "unsetenv: ");
	lib_putstr(2, elem->av[1]);
	lib_putstr(2, ": Invalid argument");
	return (-1);
}

int			ft_built_unsetenv(t_mini *elem)
{
	int		ret;

	if (lib_strcmp(elem->av[0], "unsetenv") == 0)
	{
		elem->built.ret_exit = -1;
		if (elem->ac == 1)
		{
			lib_putstr(2, "to few arguments\n");
			return (-1);
		}
		if (lib_count_char_str(elem->av[1], '=') > 0)
			return (ft_msg_error(elem));
		ret = ft_check_tab_line((const char **)elem->env, elem->av[1]);
		if (ret > -1)
		{
			if ((elem->env = lib_copy_without_line(elem->env, ret)) == NULL)
				return (-1);
		}
		elem->built.ret_exit = 0;
		return (1);
	}
	return (2);
}
