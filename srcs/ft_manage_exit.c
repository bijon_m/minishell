/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_manage_exit.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbijon <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/10 14:17:02 by mbijon            #+#    #+#             */
/*   Updated: 2018/10/08 15:40:53 by mbijon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "minishell.h"
#include "libft.h"

int			ft_manage_exit(t_mini *elem)
{
	if (elem->av != NULL && elem->av[0] != NULL)
		if (lib_strcmp(elem->av[0], "exit") == 0)
		{
			if (elem->ac > 2)
			{
				lib_putstr(2, "exit: too many arguments\n");
				return (0);
			}
			if (elem->av[1] != NULL)
				elem->built.ret_exit = lib_get_nbr(elem->av[1]);
			return (2);
		}
	return (0);
}
