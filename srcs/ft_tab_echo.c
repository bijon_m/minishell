/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tab_echo.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbijon <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/08 15:41:33 by mbijon            #+#    #+#             */
/*   Updated: 2018/10/08 16:04:25 by mbijon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "minishell.h"

void	is_bell(void)
{
	char c;

	c = '\a';
	write(1, &c, 1);
}

void	is_back(void)
{
	char	c;

	c = '\b';
	write(1, &c, 1);
}

void	is_escape(void)
{
	char	c;

	c = 27;
	write(1, &c, 1);
}

void	is_formfeed(void)
{
	char	c;

	c = '\f';
	write(1, &c, 1);
}
