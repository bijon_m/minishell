/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_tab_line.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbijon <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/15 12:47:19 by mbijon            #+#    #+#             */
/*   Updated: 2018/10/08 15:26:28 by mbijon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "libft.h"

int			ft_check_tab_line(const char **src, const char *str)
{
	int		i;

	i = 0;
	if (src != NULL)
		while (src[i])
		{
			if (lib_line_env(src[i], str) == 0)
				return (i);
			i++;
		}
	return (-1);
}
