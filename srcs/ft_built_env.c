/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_built_env.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbijon <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/14 12:17:07 by mbijon            #+#    #+#             */
/*   Updated: 2018/10/08 15:38:54 by mbijon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "minishell.h"
#include "libft.h"

int			ft_usage_env(t_mini *elem)
{
	elem->built.ret_exit = 1;
	lib_putstr(2, "env: illegal option -- ");
	if (lib_strcmp(elem->av[1], "--help") == 0)
		lib_putstr(2, "h\n");
	else
		lib_putstr(2, "e\n");
	lib_putstr(2, "usage: env[-iv][-P utilpath] [-S string] [-u name]\n");
	lib_putstr(2, "[name=value ...] [utility [argument ...]]\n");
	elem->built.ret_exit = 1;
	return (1);
}

int			ft_without_line_env(t_mini *elem)
{
	int		ret;

	if (lib_count_char_str(elem->av[2], '=') > 0)
	{
		lib_putstr(2, "env: unsetenv ");
		lib_putstr(2, elem->av[2]);
		lib_putstr(2, ": Invalid argument\n");
		return (-1);
	}
	ret = ft_check_tab_line((const char **)elem->env, elem->av[2]);
	if (ret > -1)
		if ((elem->env = lib_copy_without_line(elem->env, ret)) == NULL)
			return (-1);
	return (1);
}

static int	ft_condition(t_mini *elem)
{
	if (elem->env != NULL && elem->ac == 2 &&
		lib_strcmp(elem->av[1], "-i") == 0)
	{
		lib_free_tab(elem->env);
		elem->env = NULL;
		return (1);
	}
	return (0);
}

int			ft_built_env(t_mini *elem)
{
	int		i;

	i = 1;
	if (lib_strcmp(elem->av[0], "env") == 0)
	{
		elem->built.ret_exit = 0;
		if (ft_condition(elem) == 1)
			return (1);
		else if (elem->ac == 2 && (lib_strcmp(elem->av[1], "--help") == 0 ||
				lib_strcmp(elem->av[1], "--version") == 0))
			return (ft_usage_env(elem));
		else if (elem->ac == 3 && lib_strcmp(elem->av[1], "-u") == 0)
			return (ft_without_line_env(elem));
		else if (elem->ac == 2)
			while (elem->av[i] && lib_count_char_str(elem->av[i], '=') > 0 &&
				lib_count_char_str(elem->av[i], ' ') == 0)
				elem->env = lib_copy_tab_line(elem->env, elem->av[i++]);
		if (elem->ac == 1 || ((elem->ac > 1 && lib_count_char_str(elem->av[1],
			'=')) == 1) || (elem->ac == 2 && lib_strcmp(elem->av[2], "-v") ==
			0))
			lib_put_tab((const char **)elem->env);
		return (1);
	}
	return (2);
}
