/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_manage_line.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbijon <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/14 12:09:34 by mbijon            #+#    #+#             */
/*   Updated: 2018/10/08 15:41:10 by mbijon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "minishell.h"
#include "libft.h"

char		*ft_manage_line(const char *buf, t_mini *elem)
{
	char	*epur;

	if ((epur = lib_epur_str(buf)) == NULL)
		return (NULL);
	if ((elem->av = lib_white_space(epur, ' ')) == NULL)
		return (NULL);
	elem->ac = lib_count_line_tab((const char **)elem->av);
	return (epur);
}
