/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tab_echo_next.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbijon <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/08 15:41:44 by mbijon            #+#    #+#             */
/*   Updated: 2018/10/08 15:42:23 by mbijon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "minishell.h"

void	is_newline(void)
{
	char	c;

	c = '\n';
	write(1, &c, 1);
}

void	is_return(void)
{
	char	c;

	c = '\r';
	write(1, &c, 1);
}

void	is_horizontal(void)
{
	char	c;

	c = '\t';
	write(1, &c, 1);
}

void	is_vertical(void)
{
	char	c;

	c = '\v';
	write(1, &c, 1);
}
