/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_less.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbijon <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/13 15:10:10 by mbijon            #+#    #+#             */
/*   Updated: 2018/10/13 15:12:52 by mbijon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>
#include "minishell.h"
#include "libft.h"

char			*ft_less(void)
{
	char		*old_path;
	char		*nv_path;
	char		*buf;
	int			ret;

	buf = NULL;
	old_path = getcwd(buf, PATH_MAX);
	if ((ret = lib_count_char_str(old_path, '/')) >= 1)
	{
		nv_path = lib_strnchr(old_path, ret - 1, '/');
		return (nv_path);
	}
	return (lib_strdup(old_path));
}
