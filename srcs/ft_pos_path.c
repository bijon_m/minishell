/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pos_path.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbijon <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/15 14:57:28 by mbijon            #+#    #+#             */
/*   Updated: 2018/10/08 15:42:46 by mbijon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "libft.h"

int		ft_pos_path(const char **tab)
{
	int	i;
	int	count;

	i = 0;
	count = 0;
	if (tab != NULL)
		while (tab[i])
		{
			if (lib_strcmp(tab[i], "..") == 0)
				count++;
			i++;
		}
	return (count);
}
