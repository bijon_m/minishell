/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_built_cd.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbijon <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/22 13:08:24 by mbijon            #+#    #+#             */
/*   Updated: 2018/10/13 18:34:18 by mbijon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>
#include "minishell.h"
#include "libft.h"

static int		ft_tild(t_mini *elem)
{
	char		*nv_path;
	char		*old_path;
	char		*buf;

	buf = NULL;
	nv_path = NULL;
	if ((old_path = getcwd(buf, PATH_MAX)) == NULL)
		return (1);
	nv_path = lib_strnchr(old_path, 2, '/');
	if (chdir(nv_path) == -1)
		ft_msg_error_cd(nv_path, elem);
	return (1);
}

static char		*ft_nv_path_next(t_mini *elem, char *old_path)
{
	char		*nv_path;

	nv_path = NULL;
	if (lib_count_char_str(elem->av[1], '/') > 0)
		nv_path = lib_strrchr(elem->av[1], '/');
	nv_path = lib_strcat(old_path, nv_path);
	return (nv_path);
}
#include <stdio.h>

char			*lib_strchr(char *str, int c)
{
	int			i;

	i = 0;
	while (str[i])
	{
		if (str[i] == c)
			return (&str[i + 1]);
		i++;
	}
	return (NULL);
}
static char		*ft_condition(t_mini *elem, int ret, char *old_path,
							char *nv_path)
{
	char		*tmp;

	if (elem->av[1][0] == '~')
	{
		tmp = lib_strchr(nv_path, '/');
		nv_path = lib_strncat(lib_strdup("/Users/mbijon"), '/', lib_strdup(tmp), 2);
	}
	/*int		ret_slash;
	char	*tmp;

	if ((ret_slash = lib_count_char_str(old_path, '/')) <= ret)
	{
		free(nv_path);
		nv_path = lib_strdup("/");
	}
	else if (ret > 1)
	{
		old_path = lib_strnchr(tmp, ret_slash - ret, '/');
		nv_path = ft_nv_path_next(elem, old_path);
	}
	else if (lib_count_char_str(elem->av[1], '/') >
			lib_count_char_str(old_path, '/'))
	{
		free(nv_path);
		free(old_path);
		nv_path = lib_strdup(elem->av[1]);
	}
	else
		nv_path = lib_strncat(old_path, '/', nv_path, 2);*/
	return (nv_path);
}

static char		*ft_nv_path(t_mini *elem)
{
	char	*nv_path;
	char	*old_path;
	char	*buf;
	int		ret;
	char	**tab;

	buf = NULL;
	nv_path = NULL;
	if (elem->av[1][0] == '~' && elem->av[1][1] == '\0')
		ft_tild(elem);
	else if (elem->av[1][0] == '-' && elem->av[1][1] == '\0')
		return (ft_less());
	else
	{
		tab = lib_white_space(elem->av[1], '/');
		ret = ft_pos_path((const char **)tab);
		lib_free_tab(tab);
		if ((old_path = getcwd(buf, PATH_MAX)) == NULL)
			return (NULL);
		nv_path = lib_strdup(elem->av[1]);
		nv_path = ft_condition(elem, ret, old_path, nv_path);
	}
	return (nv_path);
}

int				ft_built_cd(t_mini *elem)
{
	char	*nv_path;
	char	*tmp;

	nv_path = NULL;
	if (lib_strcmp(elem->av[0], "cd") == 0)
	{
		if (elem->env != NULL)
		{
			if (elem->ac == 1)
				return (ft_tild(elem));
			else
			{
				if (chdir(elem->av[1]) == -1)
				{
					nv_path = ft_nv_path(elem);
					if (chdir(nv_path) == -1)
						return (ft_msg_error_cd(nv_path, elem));
					free(nv_path);
				}
			}
			return (1);
		}
	}
	return (2);
}
