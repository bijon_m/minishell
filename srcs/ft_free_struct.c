/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_free_struct.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbijon <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/08 16:39:59 by mbijon            #+#    #+#             */
/*   Updated: 2018/10/08 16:57:11 by mbijon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "libft.h"

void			ft_free_struct(t_mini *elem)
{
	lib_free_tab(elem->env);
	lib_free_tab(elem->av);
}
