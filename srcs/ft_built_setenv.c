/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_built_setenv.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbijon <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/15 13:04:40 by mbijon            #+#    #+#             */
/*   Updated: 2018/10/13 15:12:19 by mbijon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "minishell.h"
#include "libft.h"

static int	ft_condition(t_mini *elem, int ret)
{
	elem->env[ret] = NULL;
	if ((elem->env[ret] = lib_strdup(elem->av[1])) == NULL)
		return (-1);
	return (1);
}

int			ft_built_setenv(t_mini *elem)
{
	int		ret;

	if (lib_strcmp(elem->av[0], "setenv") == 0)
	{
		if (elem->ac < 2)
		{
			lib_putstr(2, "to few arguments\n");
			return (-1);
		}
		if (lib_count_char_str(elem->av[1], '=') > 1)
			return (-1);
		ret = ft_check_tab_line((const char **)elem->env, elem->av[1]);
		if (ret == -1)
		{
			if ((elem->env = lib_copy_tab_line(elem->env, elem->av[1]))
				== NULL)
				return (-1);
		}
		else
			return (ft_condition(elem, ret));
		return (1);
	}
	return (2);
}
