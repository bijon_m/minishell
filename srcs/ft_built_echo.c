/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_built_echo.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbijon <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/22 13:17:16 by mbijon            #+#    #+#             */
/*   Updated: 2018/10/08 16:04:48 by mbijon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "minishell.h"
#include "libft.h"

t_tab g_tab[] = {
	{'a', is_bell},
	{'b', is_back},
	{'e', is_escape},
	{'f', is_formfeed},
	{'n', is_newline},
	{'r', is_return},
	{'t', is_horizontal},
	{'v', is_vertical},
};

static void		ft_display(t_mini *elem, int k, int i, int j)
{
	if (elem->av[i][j] != '\"')
		write(1, &elem->av[i][j], 1);
	if (k == 8 && elem->av[i][j - 1] == '\\')
		write(1, "\\", 1);
}

int				ft_tab_echo(t_mini *elem, int j, int i, t_echo *nv)
{
	int			k;

	while (elem->av[i][j])
	{
		if (elem->av[i][j] == '\\')
		{
			j++;
			k = 0;
			while (k < 8 && elem->av[i][j - 1] == '\\')
			{
				if (g_tab[k].flag == elem->av[i][j])
				{
					g_tab[k].f();
					j += 1;
				}
				k++;
			}
		}
		ft_display(elem, k, i, j);
		j++;
	}
	if (nv->n == 0)
		lib_putstr(1, "\n");
	return (1);
}

static int		ft_display_base(t_mini *elem, int i, t_echo *nv)
{
	int			j;

	while (elem->av[i])
	{
		j = 0;
		while (elem->av[i][j])
		{
			if (elem->av[i][j] != '\"' && nv->e == 0)
				write(1, &elem->av[i][j], 1);
			j++;
		}
		i++;
		if (elem->ac > i)
			write(1, " ", 1);
	}
	if (nv->n == 0)
		lib_putstr(1, "\n");
	return (1);
}

int				ft_built_echo(t_mini *elem)
{
	t_echo		nv;
	int			i;

	if (lib_strcmp(elem->av[0], "echo") == 0)
	{
		if (elem->av[0] != NULL && lib_strcmp(elem->av[1], "$?") == 0)
		{
			lib_putnbr(elem->built.ret_exit);
			return (2);
		}
		i = ft_option_echo(elem, &nv);
		if (nv.e == 1)
			return (ft_tab_echo(elem, 0, i, &nv));
		else
			return (ft_display_base(elem, i, &nv));
	}
	return (2);
}
