/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_option_echo.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbijon <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/08 15:55:02 by mbijon            #+#    #+#             */
/*   Updated: 2018/10/08 16:06:51 by mbijon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "minishell.h"
#include "libft.h"

int			ft_option_echo(t_mini *elem, t_echo *nv)
{
	int		i;

	i = 1;
	nv->e = 0;
	nv->n = 0;
	nv->e_maj = 0;
	if (elem->av[i] != NULL)
		while (elem->av[i][0] == '-')
		{
			if (elem->av[i][1] == 'n')
				nv->n = 1;
			else if (elem->av[i][1] == 'E')
				nv->e_maj = 1;
			else if (elem->av[i][1] == 'e')
				nv->e = 1;
			i++;
		}
	if (nv->e_maj == 1 && nv->e == 1)
		nv->e = 0;
	return (i);
}
