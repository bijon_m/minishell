/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_msg_error_executable.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbijon <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/08 16:27:56 by mbijon            #+#    #+#             */
/*   Updated: 2018/10/13 15:09:36 by mbijon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "minishell.h"

void		ft_msg_error_executable(t_mini *elem, char **tab)
{
	lib_putstr(2, elem->av[0]);
	lib_putstr(2, ": command not found\n");
	lib_free_tab(tab);
}
