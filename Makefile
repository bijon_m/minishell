# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mbijon <marvin@42.fr>                      +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/06/19 03:50:33 by mbijon            #+#    #+#              #
#    Updated: 2018/10/13 15:14:23 by mbijon           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC		=	gcc

RM		=	rm -rf

NAME	=	minishell

LDFLAGS	=	-L. -lft

CFLAGS	=	-Wall -Werror -Wextra
CFLAGS	=	-I./include
CFLAGS	+=	-I./libft/include

SRCS	=	srcs/main.c \
			srcs/ft_msg_error_cd.c \
			srcs/ft_built_setenv.c \
			srcs/ft_manage_line.c \
			srcs/ft_built_env.c \
			srcs/ft_built_cd.c \
			srcs/ft_msg_error_executable.c \
			srcs/ft_free_struct.c \
			srcs/ft_manage_executable.c \
			srcs/ft_built_echo.c \
			srcs/ft_pos_path.c \
			srcs/ft_check_tab_line.c \
			srcs/ft_option_echo.c \
			srcs/ft_tab_echo.c \
			srcs/ft_tab_echo_next.c \
			srcs/ft_less.c \
			srcs/ft_built_unsetenv.c \
			srcs/ft_manage_exit.c

OBJS	=	$(SRCS:.c=.o)

all		:	$(NAME)

$(NAME)	:	$(OBJS)
			make -C libft/
			$(CC) $(OBJS) -o $(NAME) $(LDFLAGS)

clean	:
			make -C libft/ clean
			$(RM) $(OBJS)

fclean	:	clean
			make -C libft/ fclean
			$(RM) $(NAME)

re		:	fclean all

.PHONY	:	all clean fclean re
